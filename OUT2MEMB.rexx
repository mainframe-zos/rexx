/* REXX */
/* ****************************************************************** */
/* DUMP SDSF JOB OUTPUT dans des membres                              */
/*                                                                    */
/* 1 membre par JOBNAME                                               */
/* Si plusieus jobs ont le meme JOBNAME,                              */
/* les OUTPUT setont seront concatene dans le membre                  */
/*                                                                    */
/* Variables importantes (a modifier au besoin)                       */
/*   - JOBNAME (STEM) : liste des jobs a pendre en compte             */
/*   - outpds : PDS/PDSE de sortie (doit exister)                     */
/*   - isfowner : onwer des jobs                                      */
/*   - isfprefix : prefix des jobs                                    */
/* Les OUTPUT des jobs seront ecrites dans les membres :              */
/*   - outpds(JOBNAME)                                                */
/*                                                                    */
/* ATTENTION                                                          */
/* L'execution de ce REXX effacera les memebres deja existants        */
/*                                                                    */
/* ****************************************************************** */
/* verbose : 0=off ; 1=on */
verbose = 0
uid=userid()
outpds = 'MY.PDS'
/* JOBNAMEs a prendre en compte */
/* Si ajout, ne pas oublier d'incrementer JOBNAME.0 */
JOBNAME.1 = uid'AD'
JOBNAME.2 = 'NONE'
JOBNAME.0 = 2
/* ****************************************************************** */
/* SDSF */
rc=isfcalls('ON')
isfowner  = uid
isfprefix = uid'*'
Address SDSF "ISFEXEC ST"
lrc=rc
if verbose then call msgrtn
if lrc<>0 then exit 20
/* ****************************************************************** */
/* loop JOBNAME */
do i = 1 to JOBNAME.0
  /* SET LJN */
  LJN = JOBNAME.i
  if verbose then say '==>> JOBNAME = 'LJN' <<=='
  /* loop JNAME (SDSF) */
  do ix=1 to JNAME.0
    /* if JNAME == LNJ */
    if JNAME.ix = LJN then
      do
        if verbose then SAY " JOB id="ix
        Address SDSF "ISFACT ST TOKEN('"TOKEN.ix"') PARM(NP SA)"
        lrc=rc
        if verbose then call msgrtn
        if lrc<>0 then exit 20
        if verbose then
          say "Number of data sets allocated:" value(isfdsname".0")
        do jx=1 to isfddname.0          /* LOOP DDNAME */
          if verbose then say "Now reading" isfdsname.jx
          queue '###' isfdsname.jx '###'
          "EXECIO * DISKR" isfddname.jx "(STEM line. FINIS"
          if verbose then say "  Lines read:" line.0
          do kx = 1 to line.0
            queue line.kx
          end
        end
      end
  end
  /*    queue dump to member     */
  /* member name = 'outpds(LJN)' */
  n = queued()
  if verbose then say "Queue size = "n
  if n > 0 then call writemember
end
/* ****************************************************************** */
/* exit */
rc=isfcalls('OFF')
exit
/* ****************************************************************** */
/* subroutine */
/* msgrtn : Subroutine to list error messages */
msgrtn: procedure expose isfmsg isfmsg2.
  if isfmsg<>"" then
    say "isfmsg is:" isfmsg
  do ix=1 to isfmsg2.0
    say "isfmsg2."ix "is:" isfmsg2.ix
  end
  return
/* writemember */
writemember:
  lfname = outpds"("LJN")"
  if verbose then say "Queue size = "lfname
  address tso "ALLOCATE DDNAME(outdd) DSNAME(" lfname ") OLD"
  "EXECIO  0  DISKW  outdd  (OPEN"
  "EXECIO "n" DISKW  outdd"
  "EXECIO  0  DISKR  outdd  (FINIS"
  "FREE DDNAME(outdd)"
return

