# Rexx

Some Rexx script for zOS

# usage

## Add scripts
Add this scripts in a PDS member

## Execute Rexx

### standalone
```
EXEC ''&HLQ..LIB.REXX'(MYREXX) PARM1 PARM2'
```

### with ALTLIB
Alter library whith your PDS Rexx library, for exemple :
```
ALTLIB ACTIVATE APPLICATION(EXEC) DATASET('&HLQ..LIB.REXX')
```
### with ALLOCATE SYSEXEC
ALLOCATE DD SYSEXEC more **dangerous**
```
ALLOCATE DA('&HLQ..LIB.REXX') DDNAME(SYSEXEC) SHR
```
Command to show the ALLOCATE SYSEXEC modification
```
TSO ISRDDN
```

## Rexx project - how to use

### PDSMEMUP
Use for change strings in all member of a PDS.
Exemple for:
 - change all members of XXXX.MY.PDS
 - change all values of CLASS=D to CLASS=C
```
PDSMEMEX XXXX.MY.PDS PDSMEMUP CLASS=D CLASS=C
```

# Notes
## Stack
# stack
```
 ┌─────── PUSH
 |   ┌──> PULL  (PARSE UPPER PULL)
 │   │          (from keyboard if empty datastack)
 v   │
║     ║             ╮
║-----║             ┊
║     ║             ┊
║-----║             ├> datastack
║     ║             ┊
║-----║             ┊
║     ║             ╯
   ^
   |
   └───── QUEUE
```

queued() : nombre d'élément dans la queue

# DO
WHILE : test avant itération
UNTIL : test après itération
FOREVER

DO A = 1 TO 7 BY 2   ... END
DO WHILE CTR < LIMIT ... END
DO UNTIL CTR = LIMIT ... END

Leave = J'arrete la boucle directement
Iterate = Je passe à la valeur suivante

# Parse
PARSE VALUE date('E') WITH jour '/' mois '/' annee
PARSE UPPER VAR myvar m1 m2 m3 m4
PARSE UPPER VAR myvar m1 . m3 m4
                         .  => pour omettre

# datatype
IF datatype(myvar) = 'NUM' THEN ... ELSE ...

# Function
functname: procedure expose var1 var2
   ...
return
