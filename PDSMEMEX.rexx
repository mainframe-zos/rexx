/* REXX */
/* The subroutine is called for each member of the pds */
/* 2 parameters: pds name & subroutine name            */

Parse Upper Arg fdsn func rest
Say fdsn ' ' func
 If 0<>listdsi("'"fdsn"'") then
  Do
     Say 'Data set 'fdsn' not allocated'
     Exit
   End
 If 'PO'<>substr(sysdsorg,1,2) then
   Do
     Say 'Data set 'fdsn' is not a pds'
     Exit
   End
"ALLOC F(AREAD) DS('"fdsn"') SHR REUSE DSORG(PS) LRECL(256) RECFM(F B)"
'EXECIO * DISKR AREAD ( FINIS STEM DIRS.'
'FREE F(AREAD)'
Do blocknum = 1 to dirs.0
  block=dirs.blocknum
  Parse Var dirs.blocknum bl 3 block
  block=substr(block,1,c2d(bl)-2)
  Do While block<>''
    Parse Var block mbrname 9 ttr 12 c 13 block
    c=c2d(bitand(c,'1f'x))
    If mbrname='FFFFFFFFFFFFFFFF'x then
        Leave blocknum
    mbrname=strip(mbrname)
    func fdsn mbrname rest
    If RC <> 0 then Exit(RC)
    block=delstr(block,1,c*2)
  End
  If RC <> 0 then
    Do
      Say "RC passed from function: " RC
    End
End
Exit(RC)
