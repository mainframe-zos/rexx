/* REXX */
/* routine PDSMEMUP change members of a pds fromval toval          */
/* called from PDSMEMEX                                            */

/*Example: PDSMEMEX XXX.PROD.YYYY.ZZZZZ PDSMEMUP CLASS=D CLASS=C   */
/*         change all members of XXX.PROD.YYYY.ZZZZZ               */
/*         change all values of CLASS=D to CLASS=C                 */

Parse Upper Arg dsname name oldval newval rest

SAY "'"dsname'('name")'"
/* READ/WRITE USING ARRAY */
 FROM_NAME = "'"DSNAME"("NAME")'"
/* READ/WRITE USING ARRAY */
 FROM_NAME = "'"DSNAME"("NAME")'"
"ALLOCATE DDNAME(INFILE) SHR DSNAME(" FROM_NAME ")"

"EXECIO * DISKR INFILE (STEM RECD. FINIS"
HOW_MANY = RECD.0

do c = 1 to how_many
  strt = pos(oldval,recd.c)
  do while strt > 0
    say
    say recd.c
    ln = left(recd.c,strt-1)||newval||substr(recd.c,strt+length(oldval))
    recd.c = ln
    say recd.c
    strt = pos(oldval,recd.c)
  end
end c

"EXECIO " HOW_MANY " DISKW INFILE (STEM RECD. FINIS"

"FREE DDNAME(INFILE)"
Exit
